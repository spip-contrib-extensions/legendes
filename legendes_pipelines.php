<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Charger la CSS et le javascript du plugin dans l’espace public
 *
 * @param string $flux Code html des styles CSS à charger
 * @return string Code html complété
 */
function legendes_insert_head($flux) {
	$flux .= '<script src="' . _DIR_PLUGIN_LEGENDES . 'javascript/jquery.annotate.js" type="text/javascript"></script>';
	$flux .= '<link rel="stylesheet" href="' . _DIR_PLUGIN_LEGENDES . 'legendes.css" type="text/css" media="screen" />';
	return $flux;
}

/**
 * Charger les modules jQuery UI nécessaires
 *
 * @param array $plugins
 * @return array
 */
function legendes_jqueryui_plugins($plugins) {
	$plugins[] = 'jquery.ui.core';
	$plugins[] = 'jquery.ui.widget';
	$plugins[] = 'jquery.ui.mouse';
	$plugins[] = 'jquery.ui.draggable';
	$plugins[] = 'jquery.ui.resizable';
	return $plugins;
}

/**
 * Modifier les légendes lors de la rotation d'un document
 * Supprimer les légendes liées à un document supprimé
 * Mettre à jour la date de modification du document lors de la modification d'une légende associée
 *
 * @param array $flux
 * @return array
 */
function legendes_post_edition($flux) {
	// si on tourne un document, tourner les legendes associees
	if (
			isset($flux['args']['type'])
			&& $flux['args']['type'] == 'document'
			&& $flux['args']['action'] == 'tourner'
	) {
		$id_document = $flux['args']['id_objet'];
		$angle = $flux['args']['champs']['rotation'];
		$res = sql_select('id_legende', 'spip_legendes', 'id_document=' . intval($id_document));
		while ($row = sql_fetch($res)) {
			$id_legende = $row['id_legende'];
			include_spip('action/editer_legende');
			tourner_legende($id_legende, $angle);
		}
		// Invalider les caches
		include_spip('inc/invalideur');
		suivre_invalideur("id='id_document/$id_document'");
	}
	/**
	 * On supprime les légendes de documents supprimés
	 */
	if (
			isset($flux['args']['type'])
			&& $flux['args']['type'] == 'document'
			&& $flux['args']['action'] == 'supprimer_document'
		) {
		$legendes_documents = sql_select('id_legende', 'spip_legendes', 'id_document=' . intval($flux['args']['id_objet']));
		include_spip('action/editer_legende');
		while ($legende = sql_fetch($legendes_documents)) {
			supprimer_legende($legende['id_legende']);
		}
	}
	/**
	 * A la modification d'une légende, on met à jour le champs maj du document
	 */
	if (
			isset($flux['args']['type'])
			&& $flux['args']['type'] == 'legende'
			&& $flux['args']['action'] == 'modifier'
		) {
		$id_document = sql_getfetsel('id_document', 'spip_legendes', 'id_legende=' . intval($flux['args']['id_objet']));
		if (intval($id_document)) {
			include_spip('inc/modifier');
			include_spip('action/editer_document');
			document_modifier($id_document, $c = ['maj' => date('Y-m-d H:i:s')]);
		}
	}
	return $flux;
}
