<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Interfaces de la table legendes pour le compilateur
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interface
 * @return array $interface
 */
function legendes_declarer_tables_interfaces($interface) {
	$interface['table_des_tables']['legendes'] = 'legendes';

	return $interface;
}

/**
 * Déclaration de la table spip_legendes et de l'objet legendes
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables Tableau des objets déclarés
 * @return array $tables Tableau des objets complété
 */
function legendes_declarer_tables_objets_sql($tables) {
	$tables['spip_legendes'] = [
		'principale' => 'oui',
		'field' => [
			'id_legende' => 'bigint(21) NOT NULL',
			'id_document' => "bigint(21) NOT NULL DEFAULT '0'",
			'id_auteur' => "bigint(21) NOT NULL DEFAULT '0'",
			'posx' => "bigint(21) NOT NULL DEFAULT '0'",
			'posy' => "bigint(21) NOT NULL DEFAULT '0'",
			'width' => "bigint(21) NOT NULL DEFAULT '0'",
			'height' => "bigint(21) NOT NULL DEFAULT '0'",
			'texte' => 'text NOT NULL',
			'date' => "datetime NOT NULL DEFAULT '0000-00-00 00:00:00'"
		],
		'key' => [
			'PRIMARY KEY' => 'id_legende',
			'KEY id_document' => 'id_document',
			'KEY id_auteur' => 'id_auteur'
		]
	];

	return $tables;
}
