<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Upgrade des tables
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 * @return void
 */
function legendes_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [
		['maj_tables', ['spip_legendes']]
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Desinstallation
 *
 * @param string $nom_meta_base_version
 * @return void
 */
function legendes_vider_tables($nom_meta_base_version) {
	sql_drop_table('spip_legendes');
	effacer_meta($nom_meta_base_version);
	effacer_meta('legendes');
}
