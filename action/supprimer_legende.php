<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action de suppression d'une légende
 *
 * @return void
 */
function action_supprimer_legende_dist() {

	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	[$id_legende] = preg_split(',[^0-9],', $arg);
	include_spip('inc/autoriser');
	if (intval($id_legende) && autoriser('supprimer', 'legende', $id_legende)) {
		include_spip('action/editer_legende');
		supprimer_legende(intval($id_legende));
	}
}
