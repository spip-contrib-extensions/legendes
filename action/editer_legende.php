<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action editer_legende
 *
 * @param int $arg
 * @return array
 */
function action_editer_legende_dist($arg = null) {

	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// si id_legende n'est pas un nombre, c'est une creation
	if (!$id_legende = intval($arg)) {
		$id_document = _request('id_document');
		if (!$id_legende = legende_inserer($id_document)) {
			return [false,_L('echec')];
		}
	}

	$err = legende_modifier($id_legende);
	return [$id_legende,$err];
}

/**
 * Enregistre les modifications d'une légende
 *
 * @param int $id_legende
 * @param array|null $set
 * @return string|null
 */
function legende_modifier($id_legende, $set = null) {
	$err = '';

	$c = $set;
	if (!$c) {
		$c = [];
		$c['posy'] = _request('top');
		$c['posx'] = _request('left');
		$c['width'] = _request('width');
		$c['height'] = _request('height');
		$c['texte'] = _request('texte');
		$c['id_document'] = _request('id_document');
	}

	include_spip('inc/modifier');

	$err = objet_modifier_champs('legende', $id_legende, [
		'invalideur' => "id='id_legende/$id_legende'"
	], $c);

	return $err;
}

/**
 * Créer une nouvelle légende
 *
 * @pipeline_appel pre_insertion
 *
 * @param int $id_document
 * @return int|bool
 */
function legende_inserer($id_document) {
	include_spip('inc/autoriser');
	if (!autoriser('creerdans', 'legende', $id_document)) {
		return false;
	}

	$champs = [
		'id_document' => $id_document,
		'id_auteur' => $GLOBALS['visiteur_session']['id_auteur'],
		'date' => date('Y-m-d H:i:s')
	];

	// Envoyer aux plugins
	$champs = pipeline(
		'pre_insertion',
		[
			'args' => [
			'table' => 'spip_legendes',
			],
			'data' => $champs
		]
	);

	$id_legende = sql_insertq('spip_legendes', $champs);

	if (!$id_legende) {
		spip_log("legendes action insert legende : impossible d'ajouter une legende");
		return false;
	}
	return $id_legende;
}

/**
 * Supprimer une légende
 *
 * @param int $id_legende
 * @return int|bool
 */
function supprimer_legende($id_legende) {
	include_spip('inc/autoriser');
	if (!autoriser('supprimer', 'legende', $id_legende)) {
		return false;
	}

	if (intval($id_legende)) {
		sql_delete('spip_legendes', 'id_legende=' . intval($id_legende));
	}
	$id_legende = 0;
	return $id_legende;
}

/**
 * Tourner une légende
 *
 * @param int $id_legende
 * @param int $angle
 * @return string
 */
function tourner_legende($id_legende, $angle) {

	// recuperer les infos dela note à tourner
	$legende = sql_fetsel('*', 'spip_legendes', 'id_legende=' . intval($id_legende));
	$c = [];
	foreach ($legende as $key => $val) {
		$c[$key] = $val;
	}
	$n = [];

	// recuperer les infos de l'image associee
	$image = sql_fetsel('*', 'spip_documents', 'id_document=' . intval($c['id_document']));
	$largeur = $image['largeur'];
	$hauteur = $image['hauteur'];

	include_spip('inc/modifier');

	if ($angle == 0) {
		return '';
	}
	$err = '';
	if ($angle == -90) {
		$n['posx'] = $c['posy'];
		$n['posy'] = $hauteur - ($c['posx'] + $c['width']);
		$n['width'] = $c['height'];
		$n['height'] = $c['width'];
		$err .= legende_modifier($id_legende, $n);
	}
	if ($angle == 90) {
		$n['posx'] = $largeur - ($c['posy'] + $c['height']);
		$n['posy'] = $c['posx'];
		$n['width'] = $c['height'];
		$n['height'] = $c['width'];
		$err .= legende_modifier($id_legende, $n);
	}
	if ($angle == 180) {
		$n['posx'] = $largeur - ($c['posx'] + $c['width']);
		$n['posy'] = $hauteur - ($c['posy'] + $c['height']);
		$n['width'] = $c['width'];
		$n['height'] = $c['height'];
		$err .= legende_modifier($id_legende, $n);
	}

	return $err;
}
