<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-legendes?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// L
	'legendes_description' => 'Um plugin para incluir legendas nas  imagens, como no Flickr. Inspirado em [Fotonotes->http://www.fotonotes.net/] e baseado no script [jQuery Image Annotation->http://www.flipbit.co.uk/jquery-image-annotation.html].',
	'legendes_nom' => 'Legendas',
	'legendes_slogan' => 'Legendar as suas fotos',
];
