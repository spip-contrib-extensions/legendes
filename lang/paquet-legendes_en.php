<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-legendes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// L
	'legendes_description' => 'A plugin to add captions on pictures as Flickr. inspired from [Fotonotes->http://www.fotonotes.net/] and based on the script [jQuery Image Annotation->http://www.flipbit.co.uk/jquery-image-annotation.html].',
	'legendes_nom' => 'Captions',
	'legendes_slogan' => 'Add captions on photos',
];
